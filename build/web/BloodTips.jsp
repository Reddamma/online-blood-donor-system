<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;

            }
            * {
                box-sizing: border-box;
            }

            /* Style inputs */
            input[type=text], select, textarea {
                width: 100%;
                padding: 12px;
                border: 1px solid #ccc;
                margin-top: 6px;
                margin-bottom: 16px;
                resize: vertical;
            }

            input[type=submit] {
                background-color: #4CAF50;
                color: white;
                padding: 12px 20px;
                border: none;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            /* Style the container/contact section */
            .container {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 10px;
            }

            /* Create two columns that float next to eachother */
            .column {
                float: left;
                width: 50%;
                margin-top: 6px;
                padding: 20px;
            }

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
            @media screen and (max-width: 600px) {
                .column, input[type=submit] {
                    width: 100%;
                    margin-top: 0;
                    font-size:30px;
                }
            }
            .navbar {
                overflow: hidden;
                height:40px;
                background-color: #333;
            }
            .navbar a {
                float: left;
                font-size: 25px;
                color: white;
                text-align: center; 
                padding: 14px 16px;
                text-decoration: none;
            }
           input[type=submit]:hover {
                background-color: #45a049;
            }
        </style>
    </head>
    <body style="background-color: #ff6666">

        <div class="container">
            <div class="navbar" style="background-color:  #1a0033">
               <a href="index.html">HOME</a>
		 <a href="SearchDonors.jsp">SEARCH DONORS</a>
                 <a href="AboutUs.jsp">ABOUT US</a>
		 <a href="BloodTips.jsp">BLOOD TIPS</a>
                 <a href="RequestBlood.jsp">REQUEST BLOOD</a>
		 <a href="ContactUs.jsp">CONTACT US</a>   
            </div>    
            </div>
            <div style="text-align:center">
                <h2>Blood  Tips</h2>
                <p>Swing by for a cup of coffee, or leave us a message:</p>
            </div>
            <div class="row">
                <div class="control-label col-sm-5" align="top">
                    <img class="mySlide" src="Quotation.png" style="width:100%">
                </div>
                <div class="column">
                    <div class="control-label col-sm-6" align="top"style="width:100%" >
                        <form action="blood tips" method="post">
                            <h2>Safety Tips:</h2>
                            <p style="width:100%">Blood donation never asks to be rich or poor, any healthy person can donate blood.</p>
                            <ul>
                                <li style="width:100%">Don't let fools or mosquitoes suck your blood, put it to good use. Donate blood and save a life. </li>
                                <li style="width:100%">Your blood donation is the best social help</li>
                                <li style="width:100%">Your blood donation can give a precious smile to someone's face.</li>
                                <li style="width:100%">Opportunities knock the door sometimes, so don't let it go and donate blood!</li>
                                <li style="width:100%">Do you feel you don't have much to offer? You have the most precious resource of all: the ability to save a life by donating blood! Help share this invaluable gift with someone in need.</li>
                                <li style="width:100%">The blood you donate gives someone another chance at life. One day that someone may be a close relative, a friend, a loved one or even you.</li>
                                <li style="width:100%">Blood donation never asks to be rich or poor, any healthy person can donate blood.</li>
                            </ul><br>
                            <h2>Precautions:</h2>
                            <ul>
                                <li style="width:100%">Drink extra water and fluids.</li>
                                <li style="width:100%">Avoid caffeinated beverages.</li>
                                <li style="width:100%">Eat well to reduce the risk of reactions to donation..</li>
                                <li style="width:100%">Eating foods high in iron is advisable.This is especially true for recently-menstruated women.</li>
                                <li style="width:100%">To the young and healthy it's no loss. To sick it's hope of life. Donate Blood to give back life.</li>
                                <li style="width:100%">A single pint can save three lives, a single gesture can create a million smiles.</li>
                                <li style="width:100%">Blood is a life, pass it on!</li>
                                <li style="width:100%">Blood donation is the way to stay healthy.</li>
                            </ul>
                            <div><a href="Login.html"></a>
                            </div>
                        </form>
                        </body>

                        </html>
 
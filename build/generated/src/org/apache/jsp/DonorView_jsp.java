package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Base64;
import java.sql.*;

public final class DonorView_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

 String driver = "oracle.jdbc.driver.OracleDriver";
String url = "jdbc:oracle:thin:@localhost:1521:xe";
String username = "BloodDonor";
String password = "123";
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');

    try {
        Class.forName(driver);
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }
    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;

      out.write("\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <style>\n");
      out.write("            .navbar {\n");
      out.write("                overflow: hidden;\n");
      out.write("                height:80px;\n");
      out.write("                background-color: #333;\n");
      out.write("            }\n");
      out.write("            .navbar a {\n");
      out.write("                float: left;\n");
      out.write("                font-size: 30px;\n");
      out.write("                color: white;\n");
      out.write("                text-align: center;\n");
      out.write("                padding: 14px 16px;\n");
      out.write("                text-decoration: none;\n");
      out.write("            }\n");
      out.write("            .dropdown {\n");
      out.write("                float: left;\n");
      out.write("                overflow: hidden;\n");
      out.write("            }\n");
      out.write("            .dropdown .dropbtn {\n");
      out.write("                font-size: 30px;  \n");
      out.write("                border: none;\n");
      out.write("                outline: none;\n");
      out.write("                color: white;\n");
      out.write("                padding: 14px 16px;\n");
      out.write("                background-color: inherit;\n");
      out.write("                font-family: inherit;\n");
      out.write("                margin: 0;\n");
      out.write("            }\n");
      out.write("            .navbar a:hover, .dropdown:hover .dropbtn {\n");
      out.write("                background-color: red;\n");
      out.write("            }\n");
      out.write("            .dropdown-content {\n");
      out.write("                display: none;\n");
      out.write("                position: absolute;\n");
      out.write("                background-color: #f9f9f9;\n");
      out.write("                min-width: 160px;\n");
      out.write("                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);\n");
      out.write("                z-index: 1;\n");
      out.write("            }\n");
      out.write("            .box{\n");
      out.write("                width: 700px;\n");
      out.write("                height: 70px;\n");
      out.write("                padding: 20px;\n");
      out.write("                margin: 100px auto;\n");
      out.write("                margin-top: auto;\n");
      out.write("                background-position: center;\n");
      out.write("                box-shadow:0px grey;\n");
      out.write("                border:10px ;\n");
      out.write("                background-image:url(image5.jpg);\n");
      out.write("                background-image:url(image6.jpg);\n");
      out.write("            }\n");
      out.write("            .mySlides {\n");
      out.write("                display:none;\n");
      out.write("                margin-left:initial;\n");
      out.write("                margin-right:inherit;\n");
      out.write("                height:250px;\n");
      out.write("            }\n");
      out.write("            .dropdown-content a {\n");
      out.write("                float: none;\n");
      out.write("                color: black;\n");
      out.write("                padding: 12px 16px;\n");
      out.write("                text-decoration: none;\n");
      out.write("                display: block;\n");
      out.write("                text-align: left;\n");
      out.write("            }\n");
      out.write("            .dropdown-content a:hover {\n");
      out.write("                background-color: #ddd;\n");
      out.write("            }\n");
      out.write("            .dropdown:hover .dropdown-content {\n");
      out.write("                display: block;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("    </head>\n");
      out.write("    <body style=\"background-color:  #ff6666\">\n");
      out.write("        <img class=\"mySlides\" src=\"SaveBlood.jpg\" style=\"width: 100%\">\n");
      out.write("        <img class=\"mySlides\" src=\"Donation.jpg\" style=\"width:100%\">\n");
      out.write("        <img class=\"mySlides\" src=\"DonateBlood.jpg\" style=\"width:100%\">\n");
      out.write("        <img class=\"mySlides\" src=\"RepresentsFlow.jpg\" style=\"width: 100%\">\n");
      out.write("        <img class=\"mySlides\" src=\"quotation.png\" style=\"width:100%\">\n");
      out.write("        <img class=\"mySlides\" src=\"BloodTypes.jpg\" style=\"width:100%\">\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div style=\"text-align:center\">\n");
      out.write("                <h2>SEARCH RESULTS</h2>\n");
      out.write("                <p>You need a big heart and free mind for blood donation and not money and strength.</p>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"column\">\n");
      out.write("                <div class=\"control-label col-sm-6\" align=\"top\" ><br></br>\n");
      out.write("                    <table border=\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td>\n");
      out.write("                                <h2>User Name</h2>\n");
      out.write("                            </td>\n");
      out.write("                            <td>\n");
      out.write("                                <h2>Blood Group</h2>\n");
      out.write("                            </td>\n");
      out.write("                            <td>\n");
      out.write("                                <h2>Email</h2>\n");
      out.write("                            </td>\n");
      out.write("                            <td>\n");
      out.write("                                <h2>Mobile Number</h2>\n");
      out.write("                            </td>\n");
      out.write("                            <td>\n");
      out.write("                                <h2>Address</h2>\n");
      out.write("                            </td>\n");
      out.write("                            <td>\n");
      out.write("                                <h2>Contact</h2>\n");
      out.write("                            </td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            try {
                                connection = DriverManager.getConnection(url, username, password);
                                statement = connection.createStatement();
                                String Address = request.getParameter("address");
                                String Bloodgroup = request.getParameter("BloodGroup");
                                System.out.println("address" + Address);
                                System.out.println("BloodGroup" + Bloodgroup);
                                String sql = "select * from donor where address like '%" + Address + "%' and bloodgroup like '%" + Bloodgroup + "%'";
                                System.out.println(sql);
                                resultSet = statement.executeQuery(sql);
                                while (resultSet.next()) {
                        
      out.write("\n");
      out.write("                        <tr>\n");
      out.write("                            <td>");
      out.print(resultSet.getString("username"));
      out.write("</td>\n");
      out.write("                            <td>");
      out.print(resultSet.getString("BloodGroup"));
      out.write("</td>\n");
      out.write("                            <td>");
      out.print(resultSet.getString("email"));
      out.write("</td>\n");
      out.write("                            <td>");
      out.print(resultSet.getString("mobilenumber"));
      out.write("</td>\n");
      out.write("                            <td>");
      out.print(resultSet.getString("address"));
      out.write("</td>\n");
      out.write("                            <td><a href=\"EmailForm.jsp?id=");
      out.print(resultSet.getString("donorid"));
      out.write("\">Contact Details</a></td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                                }
                                connection.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        
      out.write("\n");
      out.write("                </div>\n");
      out.write("                </table>\n");
      out.write("\n");
      out.write("                </body>\n");
      out.write("                <script>\n");
      out.write("                    var myIndex = 0;\n");
      out.write("                    carousel();\n");
      out.write("                    function carousel() {\n");
      out.write("                        var i;\n");
      out.write("                        var x = document.getElementsByClassName(\"mySlides\");\n");
      out.write("                        for (i = 0; i < x.length; i++) {\n");
      out.write("                            x[i].style.display = \"none\";\n");
      out.write("                        }\n");
      out.write("                        myIndex++;\n");
      out.write("                        if (myIndex > x.length) {\n");
      out.write("                            myIndex = 1\n");
      out.write("                        }\n");
      out.write("                        x[myIndex - 1].style.display = \"block\";\n");
      out.write("                        setTimeout(carousel, 1000); // Change image every 2 seconds\n");
      out.write("                    }\n");
      out.write("                </script>\n");
      out.write("\n");
      out.write("                </html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

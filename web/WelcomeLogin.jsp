<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, Helvetica, sans-serif;

            }
            * {
                box-sizing: border-box;
            }

            /* Style inputs */
            input[type=text], select, textarea {
                width: 100%;
                padding: 12px;
                border: 1px solid #ccc;
                margin-top: 6px;
                margin-bottom: 16px;
                resize: vertical;
            }

            input[type=submit] {
                background-color: #4CAF50;
                color: white;
                padding: 12px 20px;
                border: none;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            /* Style the container/contact section */
            .container {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 10px;
            }

            /* Create two columns that float next to eachother */
            .column {
                float: left;
                width: 50%;
                margin-top: 6px;
                padding: 20px;
            }

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
            @media screen and (max-width: 600px) {
                .column, input[type=submit] {
                    width: 100%;
                    margin-top: 0;
                    font-size:30px;
                }
            }

        </style>
    <html>
        <head>
            <style>
                .navbar {
                    overflow: hidden;
                    height:80px;
                    background-color: #333;
                }
                .navbar a {
                    float: left;
                    font-size: 20px;
                    color: white;
                    text-align: center;
                    padding: 14px 16px;
                    text-decoration: none;
                }
                
            </style>
        </head>
        <body style="background-color:deeppink">

            <div class="container">
                <div style="text-align:center">
                    <h2>Welcome To Blood Donor System</h2>
                    <p>Swing by for a cup of coffee, or leave us a message:</p>
                </div>
                <div class="row">
                    <div class="control-label col-sm-6" align="top">
                        <img class="mySlide" src="quiz.png" style="width: 100%">
                    </div>
                    <div class="column">
                        <div class="control-label col-sm-6" align="top"style="width:100%" >
                            <form action="Welcome" method="post">
                                <h2>Welcome</h2>
                                <p style="width:100%">Blood donation never asks to be rich or poor, any healthy person can donate blood.</p>
                                <ul>
                                    <li style="width:100%">In order to maintain accurate information and facilitate easy access, we recommend  that you update your profile with any recent changes such as a change in the contact number, email id or may be with the date of your most recent blood donation.</li>
                                    <li style="width:100%">Your blood donation is the best social help</li>
                                    <li style="width:100%">You may also help us spread the word by referring us to your friends.</li>
                                    <li style="width:100%">Opportunities knock the door sometimes, so don't let it go and donate blood!</li>
                                    <li style="width:100%">If you have any feedback, comments or suggestion, please feel free to share them through our contact us section. Help share this invaluable gift with someone in need.</li>
                                </ul><br></br>
                                <div class="navbar" style="background-color:  #1a0033">
                                    <a href="PatientView.jsp">Blood Requests</a>
                                    <a href="QuickTip.jsp">Quick Tip</a>

                                    <div><a href="Login.html"></a>
                                    </div>
                            </form>
                            </body>
                            </html>

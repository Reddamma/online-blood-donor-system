package BloodDonor;

 import java.io.IOException;

 import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 
import Bean.DonorLoginBean;
import Dao.DonorLoginDao;
 @WebServlet(urlPatterns="/donor",name="DonorLoginServlet")
public class DonorLoginServlet extends HttpServlet {
 
public DonorLoginServlet() {
 }
 
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

String username = request.getParameter("username");
System.out.println("user name " + request.getParameter("username")); 
String Password = request.getParameter("password");
System.out.println("password " + request.getParameter("password"));

DonorLoginBean loginBean = new DonorLoginBean();
 
loginBean.setUserName(username); 
 loginBean.setPassword(Password);
 
DonorLoginDao loginDao = new DonorLoginDao();
 
String userValidate = loginDao.authenticateUser(loginBean); 
 
if(userValidate.equals("SUCCESS")) 
 {
 request.setAttribute("username", username); 
 request.getRequestDispatcher("/WelcomeLogin.jsp").forward(request, response);
 }
 else
 {
 request.setAttribute("errMessage", userValidate);
 request.getRequestDispatcher("/DonorLogin.jsp").forward(request, response);
 }
 }
 
}
